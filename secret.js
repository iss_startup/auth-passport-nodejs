module.exports = {
    authProviders:{
        facebook:{
            clientID: 'MY_CLIENT_ID',
            clientSecret: 'MY_SECRET',
        },
        google:{
            clientID:'MY_ID',
            clientSecret: 'MY_SECRET',
        },
    },
};
