var express = require('express'),
    handlebars = require('express-handlebars'),
    passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;


var app = express();

app.engine('handlebars', handlebars({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true}));

app.use(passport.initialize());
app.use(passport.session());

////////////////////////////
//     Authentication
//////////////////////////
var credentials = require('./secret.js').authProviders;

passport.serializeUser(function(user, done){
    done(null, user);
});

passport.deserializeUser(function(id, done){
    done(null, id);
});

passport.use(new FacebookStrategy({
            clientID: credentials.facebook.clientID,
            clientSecret: credentials.facebook.clientSecret,
            callbackURL: '/auth/facebook/callback',
        }, function(accessToken, refreshToken, profile, done){
            //if Authenticates successful, you will be return an access Token
            //which then can be used for API access

            //Typically add a step to check if a user is in your databse,
            //if not, create a new user
            //User.findOrCreate(..., function(err, user) {
            //if (err) { return done(err); }

            done(null, profile);
        }));

passport.use(new GoogleStrategy({
                clientID: credentials.google.clientID,
                clientSecret: credentials.google.clientSecret,
                callbackURL: '/auth/google/callback',
            }, function(accessToken, refreshToken, profile, done){
                // User.findOne({authId: authId},function(err, user){
                //     if (err) return done(err, null);
                //     if (user) return done(null, user);
                //}
                  
                        done(null, profile);
            }));


app.get('/auth/facebook', passport.authenticate('facebook'));

app.get('/auth/facebook/callback',
  passport.authenticate('facebook', { successRedirect: '/main',
                                      failureRedirect: '/unauthorized' }));

app.get('/auth/google', function(req, res, next){
    if(req.query.redirect) req.session.authRedirect = req.query.redirect;
    passport.authenticate('google', {scope : 'profile'})(req, res, next);
});

app.get('/auth/google/callback', passport.authenticate('google', { successRedirect: '/main',failureRedirect: '/unauthorized' }));

////////////////////////////////////
// Routes
//////////////////////////////////
app.get('/main', function(req, res){
    if(!req.user) return res.redirect(403, '/unauthorized');
    res.render('profile', {name: req.user.displayName});
});

app.get('/unauthorized', function(req, res){
    res.status(403).send('You do not have permission to access this page!');
});



app.use(express.static(__dirname + '/public'));

app.set('port', process.env.APP_PORT || 3000);
app.listen(app.get('port'), function(){
    console.log('Application started on ' + app.get('port'));
});